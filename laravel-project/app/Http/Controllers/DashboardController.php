<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboard;
use App\Kategori;
use App\Slider;

class DashboardController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
        $kategori = Kategori::all();
        return view('admin.dashboard.index', compact('slider', 'kategori'));
    }
}
