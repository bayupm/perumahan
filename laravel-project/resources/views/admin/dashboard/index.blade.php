@extends('backend.konten')

@section('judul', 'Dashboard')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="alert alert-primary" role="alert">
                <h5>Selamat Datang</h5>
            </div>
        </div> 
        <div class="col-md-4">
        <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Kategori</span>
                <span class="info-box-number">{{$kategori->count()}}</span>
              </div>
            </div>
        </div>
        <div class="col-md-4">
        <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Slider</span>
                <span class="info-box-number">{{$slider->count()}}</span>
              </div>
            </div>
        </div>
        <div class="col-md-4">
        <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Postingan</span>
                <span class="info-box-number">1,410</span>
              </div>
            </div>
        </div>    
    </div>
</div>

@endsection