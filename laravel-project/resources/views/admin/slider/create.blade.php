@extends('backend.konten')

@section('judul', 'Tambah Slider')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('slider.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="formGroupExampleInput">Tambahkan Nama Slider</label>
                            <input type="text" class="form-control" name="nama_gambar" id="formGroupExampleInput" placeholder="Masukkan Nama Kategori">
                            <div class="invalid-feedback">
                                Tambahkan slider.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Tambahkan File Gambar</label>
                            <input type="file" class="form-control-file" name="gambar" id="exampleFormControlFile1">
                        </div>
                        <a href="{{route('slider.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan Slider</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection