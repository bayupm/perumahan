@extends('backend.konten')

@section('judul', 'Edit Slider')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('slider.update', $slider->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="formGroupExampleInput">Edit Nama Slider</label>
                            <input type="text" class="form-control" value="{{$slider->nama_gambar}}" name="nama_gambar" id="formGroupExampleInput" placeholder="Masukkan Nama Kategori">
                            <div class="invalid-feedback">
                                Tambahkan slider.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Tambahkan File Gambar</label>
                            <input type="file" class="form-control-file" name="gambar" id="exampleFormControlFile1">
                        </div>

                        <div class="form-group">
                            <img src="{{asset($slider->gambar)}}" width="20%" height="30%">
                        </div>
                        <a href="{{route('slider.index')}}" class="btn btn-primary btn-sm">Kembali</a>
                        <button type="submit" class="btn btn-success btn-sm">Simpan Slider</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>

@endsection