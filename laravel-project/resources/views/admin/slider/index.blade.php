@extends('backend.konten')

@section('judul', 'Slider')

@section('isikonten')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-2">
            <div class="mb-2 float-lg-left">
            <a href="{{route('slider.create')}}" class="btn btn-primary">Tambah Slider</a>
            </div>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Slider</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slider as $item)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$item -> nama_gambar}}</td>
                            <td>
                                <img src="{{asset( $item->gambar )}}" alt="" style="width:150px;">
                            </td>
                            <td width="15%">
                                <form action="{{route('slider.destroy',$item->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <a href="{{route('slider.edit', $item->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$slider->links()}}
        </div>
    </div>
</div>

@endsection